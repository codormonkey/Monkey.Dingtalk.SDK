using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.Department;
using Monkey.Dingtalk.SDK.Department.Dto;

namespace ApiTest.Controllers
{
  /// <summary>
  /// 钉钉部门接口
  /// </summary>
  [ApiController]
  [Route("department")]
  public class DepartmentController : ControllerBase
  {
    private readonly DepartmentApi _departmentApi;

    public DepartmentController(DepartmentApi departmentApi) {
      _departmentApi = departmentApi;
    }

    /// <summary>
    /// 查询钉钉部门
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("list")]
    public async Task<DepartmentResult> ListDepartment() {
      return await _departmentApi.DepartmentList();
    }
  }
}