using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.Helper;
using Monkey.Dingtalk.SDK.Report;
using Monkey.Dingtalk.SDK.Report.Dto;

namespace ApiTest.Controllers
{
  [ApiController]
  [Route("report")]
  public class ReportController : ControllerBase
  {
    private readonly ReportApi _reportApi;

    public ReportController(ReportApi reportApi) {
      _reportApi = reportApi;
    }

    [HttpGet]
    [Route("")]
    public async Task<ReportListResult> GetList() {
      var now   = DateTime.Now;
      var start = now.AddDays(-1).ConvertToTimeStamp();
      var end   = now.ConvertToTimeStamp();
      return await _reportApi.List(start, end);
    }
  }
}