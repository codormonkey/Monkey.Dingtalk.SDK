﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.Notice;
using Monkey.Dingtalk.SDK.Notice.Dto;
using Monkey.Dingtalk.SDK.Notice.Dto.Enum;

namespace ApiTest.Controllers
{
  [ApiController]
  [Route("notice")]
  public class NoticeController : ControllerBase
  {
    private readonly NoticeApi _noticeApi;

    public NoticeController(NoticeApi noticeApi) {
      _noticeApi = noticeApi;
    }

    [HttpPost]
    [Route("send")]
    public async Task<NoticeJobResult> Send() {
      var userIdList = new List<string> {"086413372226266231", "123"};

      var deptIdList = new List<string> {"1"};

      var msg = new TextNoticeMsg();
      msg.SetContent(DateTime.Now.ToString(CultureInfo.CurrentCulture));
      return await _noticeApi.SendNotice(899006186, null, deptIdList, false, msg);
    }

    [HttpGet]
    [Route("{id}")]
    public async Task<NoticeSendResult> GetResult([FromRoute] long id) {
      return await _noticeApi.GetNoticeResult(899006186, id);
    }
  }
}