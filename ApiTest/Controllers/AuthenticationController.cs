using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.Authentication;
using Monkey.Dingtalk.SDK.Authentication.Dto;

namespace ApiTest.Controllers
{
  [ApiController]
  [Route("authentication")]
  public class AuthenticationController : ControllerBase
  {
    private readonly AuthenticationApi _authenticationApi;

    public AuthenticationController(AuthenticationApi authenticationApi) {
      _authenticationApi = authenticationApi;
    }

    [HttpGet]
    [Route("user")]
    public async Task<AuthenticationUserInfo> GetUserInfo([FromQuery] string code) {
      return await _authenticationApi.GetUserInfo(code);
    }
  }
}