using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.Token;
using Monkey.Dingtalk.SDK.Token.Dto;

namespace ApiTest.Controllers
{
  [ApiController]
  [Route("token")]
  public class TokenController : ControllerBase
  {
    private readonly TokenApi     _tokenApi;

    public TokenController(TokenApi tokenApi) {
      _tokenApi = tokenApi;
    }

    [HttpGet]
    [Route("corp-token")]
    public async Task<GetTokenResult> GetCorpToken() {
      return await _tokenApi.GetCorpToken();
    }
  }
}