using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monkey.Dingtalk.SDK.User;
using Monkey.Dingtalk.SDK.User.Dto;

namespace ApiTest.Controllers
{
  /// <summary>
  /// 钉钉人员接口
  /// </summary>
  [ApiController]
  [Route("user")]
  public class UserController : ControllerBase
  {
    private readonly UserApi _userApi;

    public UserController(UserApi userApi) {
      _userApi = userApi;
    }

    /// <summary>
    /// 查询钉钉员工
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("list")]
    public async Task<GetDeptMemberUserDetailsResult> ListUser([FromQuery] long id) {
      return await _userApi.GetDeptMemberUserDetails(
               new GetDeptMemberUserDetailsInput {DepartmentId = id, Offset = 0, Size = 100,}
             );
    }
  }
}