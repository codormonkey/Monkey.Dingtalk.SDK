using Monkey.Dingtalk.SDK.Authentication.Dto.Enum;
using Newtonsoft.Json;

namespace Monkey.Dingtalk.SDK.Authentication.Dto
{
  public class AuthenticationUserInfo : ApiResultBase
  {
    [JsonProperty("userid")]
    public string UserId { get; set; }

    [JsonProperty("name")]
    public string Name { get; set; }

    [JsonProperty("is_sys")]
    public bool IsSys { get; set; }

    [JsonProperty("sys_level")]
    public SysLevel SysLevel { get; set; }
  }
}