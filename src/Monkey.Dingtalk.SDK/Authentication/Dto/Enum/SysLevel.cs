using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Monkey.Dingtalk.SDK.Authentication.Dto.Enum
{
  /// <summary>
  /// 级别
  /// </summary>
  public enum SysLevel
  {
    /// <summary>
    /// 其他
    /// </summary>
    [Display(Name = "其他")]
    Other = 0,

    /// <summary>
    /// 主管理员
    /// </summary>
    [Display(Name = "主管理员")]
    Admin = 1,

    /// <summary>
    /// 子管理员
    /// </summary>
    [Display(Name = "子管理员")]
    SubAdmin = 2,

    /// <summary>
    /// 老板
    /// </summary>
    [Display(Name = "老板")]
    Boss = 100,
  }
}