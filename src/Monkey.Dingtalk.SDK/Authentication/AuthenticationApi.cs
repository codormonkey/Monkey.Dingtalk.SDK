using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Monkey.Dingtalk.SDK.Authentication.Dto;

namespace Monkey.Dingtalk.SDK.Authentication
{
  public class AuthenticationApi : ApiBase
  {
    public AuthenticationApi(ILogger<ApiBase> logger, IServiceProvider serviceProvider) :
      base(logger, serviceProvider) { }

    /// <summary>
    /// 通过免登授权码获取用户信息
    /// </summary>
    /// <param name="code">免登授权码</param>
    /// <returns></returns>
    public async Task<AuthenticationUserInfo> GetUserInfo(string code) {
      return await Get<AuthenticationUserInfo>($"user/getuserinfo?access_token={{ACCESS_TOKEN}}&code={code}");
    }
  }
}