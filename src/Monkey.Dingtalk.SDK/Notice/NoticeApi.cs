﻿// ======================================================================
//   
//           Copyright (C) 2019-2020 湖南心莱信息科技有限公司    
//           All rights reserved
//   
//           filename : NoticeApi.cs
//           description :
//   
//           created by 雪雁 at  2019-03-13 10:03
//           Mail: wenqiang.li@xin-lai.com
//           QQ群：85318032（技术交流）
//           Blog：http://www.cnblogs.com/codelove/
//           GitHub：https://github.com/xin-lai
//           Home：http://xin-lai.com
//   
// ======================================================================

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Monkey.Dingtalk.SDK.Notice.Dto;
using Newtonsoft.Json;

namespace Monkey.Dingtalk.SDK.Notice
{
  public class NoticeApi : ApiBase
  {
    public NoticeApi(ILogger<NoticeApi> logger, IServiceProvider serviceProvider) : base(logger, serviceProvider) { }

    /// <summary>
    /// 发送消息
    /// 同一个微应用相同消息的内容同一个用户一天只能接收一次。
    /// 同一个微应用给同一个用户发送消息，如果是ISV应用开发方式一天不得超过50次；如果是企业内部开发方式，此上限为500次。
    /// 该接口是异步发送消息，接口返回成功并不表示用户一定会收到消息，需要通过“查询工作通知消息的发送结果”接口查询是否给用户发送成功。
    /// </summary>
    /// <param name="agentId">应用agentId</param>
    /// <param name="userIdList">接收者的企业内部用户的userid列表，最大用户列表长度：100</param>
    /// <param name="deptIdList">接收者的部门id列表，最大列表长度：20,接收者是部门id下(包括子部门下)的所有用户</param>
    /// <param name="toAllUser">是否发送给企业全部用户</param>
    /// <param name="msg">消息内容，消息类型和样例参考<a href="https://ding-doc.dingtalk.com/doc#/serverapi2/ye8tup">消息类型与数据格式</a>。最长不超过2048个字节</param>
    /// <returns></returns>
    public async Task<NoticeJobResult> SendNotice(int  agentId,   List<string>? userIdList, List<string>? deptIdList,
                                                  bool toAllUser, NoticeMsg     msg) {
      string userList = null;
      if (userIdList != null && userIdList.Count > 0) {
        userList = string.Join(",", userIdList);
      }

      string deptList = null;
      if (deptIdList != null && deptIdList.Count > 0) {
        deptList = string.Join(",", deptIdList);
      }

      return await Post<NoticeJobResult>(
               "topapi/message/corpconversation/asyncsend_v2?access_token={ACCESS_TOKEN}",
               JsonConvert.SerializeObject(
                 new {
                   agent_id     = agentId,
                   userid_list  = userList,
                   dept_id_list = deptList,
                   to_all_user  = toAllUser,
                   msg,
                 }
               )
             );
    }

    /// <summary>
    /// 查询工作通知消息的发送结果
    /// </summary>
    /// <param name="agentId">应用的agentid</param>
    /// <param name="taskId">钉钉返回的任务id。仅支持查询24小时内的任务id</param>
    /// <returns></returns>
    public async Task<NoticeSendResult> GetNoticeResult(int agentId, long taskId) {
      return await Post<NoticeSendResult>(
               "topapi/message/corpconversation/getsendresult?access_token={ACCESS_TOKEN}",
               JsonConvert.SerializeObject(
                 new {
                   agent_id     = agentId,
                   task_id = taskId,
                 }
               )
             );
    }
  }
}