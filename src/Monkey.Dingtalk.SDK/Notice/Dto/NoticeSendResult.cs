using System.Collections.Generic;
using Newtonsoft.Json;

namespace Monkey.Dingtalk.SDK.Notice.Dto
{
  public class NoticeSendResult : ApiResultBase
  {
    [JsonProperty("send_result")]
    public ResultDetail SendResult { get; set; }

    public class ResultDetail
    {
      /// <summary>
      /// 无效的用户id
      /// </summary>
      [JsonProperty("invalid_user_id_list")]
      public List<string> InvalidUserIdList { get; set; }

      /// <summary>
      /// 因发送消息超过上限而被流控过滤后实际未发送的userid。未被限流的接收者仍会被收到消息。限流规则包括：
      /// 1.同一个微应用相同消息的内容同一个用户一天只能接收一次
      /// 2.同一个微应用给同一个用户发送消息，
      /// 如果是第三方企业应用，一天最多为50次；
      /// 如果是企业内部开发方式，一天最多为500次
      /// </summary>
      [JsonProperty("forbidden_user_id_list")]
      public List<string> ForbiddenUserIdList { get; set; }

      /// <summary>
      /// 推送被禁止的具体原因列表
      /// </summary>
      [JsonProperty("forbidden_list")]
      public List<Forbidden> ForbiddenList { get; set; }

      /// <summary>
      /// 发送失败的用户id
      /// </summary>
      [JsonProperty("failed_user_id_list")]
      public List<string> FailedUserIdList { get; set; }

      /// <summary>
      /// 已读消息的用户id
      /// </summary>
      [JsonProperty("read_user_id_list")]
      public List<string> ReadUserIdList { get; set; }

      /// <summary>
      /// 未读消息的用户id
      /// </summary>
      [JsonProperty("unread_user_id_list")]
      public List<string> UnreadUserIdList { get; set; }

      /// <summary>
      /// 无效的部门id
      /// </summary>
      [JsonProperty("invalid_dept_id_list")]
      public List<string> InvalidDeptIdList { get; set; }
    }
    
    public class Forbidden
    {
      /// <summary>
      /// 流控code。包括以下code：
      /// 143105表示企业自建应用每日推送给用户的消息超过上限
      /// 143106表示企业自建应用推送给用户的消息重复
      /// </summary>
      [JsonProperty("code")]
      public string Code { get; set; }

      /// <summary>
      /// 流控阀值
      /// </summary>
      [JsonProperty("count")]
      public string Count { get; set; }

      /// <summary>
      /// 被流控员工userId
      /// </summary>
      [JsonProperty("userid")]
      public string Userid { get; set; }
    }
  }
}