﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Monkey.Dingtalk.SDK.Notice.Dto.Enum
{
  /// <summary>
  /// 消息类型
  /// </summary>
  public enum MsgTypeEnum
  {
    /// <summary>
    /// 上班
    /// </summary>
    [Display(Name     = "文本消息")]
    [EnumMember(Value = "text")]
    Text = 0,

    /// <summary>
    /// 下班
    /// </summary>
    [Display(Name     = "图片消息")]
    [EnumMember(Value = "image")]
    Image = 1,

    /// <summary>
    /// 语音消息
    /// </summary>
    [Display(Name     = "语音消息")]
    [EnumMember(Value = "voice")]
    Voice = 2,

    /// <summary>
    /// 文件消息
    /// </summary>
    [Display(Name     = "文件消息")]
    [EnumMember(Value = "file")]
    File = 3,

    /// <summary>
    /// 链接消息
    /// </summary>
    [Display(Name     = "链接消息")]
    [EnumMember(Value = "link")]
    Link = 4,

    /// <summary>
    /// OA消息
    /// </summary>
    [Display(Name     = "OA消息")]
    [EnumMember(Value = "oa")]
    Oa = 5,

    /// <summary>
    /// markdown消息
    /// </summary>
    [Display(Name     = "markdown消息")]
    [EnumMember(Value = "markdown")]
    Markdown = 6,

    /// <summary>
    /// 卡片消息
    /// </summary>
    [Display(Name     = "卡片消息")]
    [EnumMember(Value = "action_card")]
    ActionCard = 7,
  }
}