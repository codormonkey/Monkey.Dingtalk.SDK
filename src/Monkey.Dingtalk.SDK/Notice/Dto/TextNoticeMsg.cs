﻿using Monkey.Dingtalk.SDK.Notice.Dto.Enum;
using Newtonsoft.Json;

namespace Monkey.Dingtalk.SDK.Notice.Dto
{
  public class TextNoticeMsg : NoticeMsg
  {
    public override MsgTypeEnum MsgType => MsgTypeEnum.Text;

    [JsonProperty("content")]
    public string Content { get; set; }

    [JsonProperty("text")]
    public MsgContent Text { get; set; }

    public void SetContent(string content) {
      Text         ??= new MsgContent();
      Text.Content =   content;
    }

    public class MsgContent
    {
      [JsonProperty("content")]
      public string Content;
    }
  }
}