﻿using Monkey.Dingtalk.SDK.Notice.Dto.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Monkey.Dingtalk.SDK.Notice.Dto
{
  public abstract class NoticeMsg
  {
    [JsonProperty("msgtype")]
    [JsonConverter(typeof(StringEnumConverter))]
    public virtual MsgTypeEnum MsgType { get; }
  }
}