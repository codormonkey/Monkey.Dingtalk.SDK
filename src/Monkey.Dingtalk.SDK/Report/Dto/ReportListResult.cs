using System.Collections.Generic;
using Newtonsoft.Json;
using RestSharp;

namespace Monkey.Dingtalk.SDK.Report.Dto
{
  public class ReportListResult : ApiResultBase
  {
    [JsonProperty("result")]
    public ReportResult Result { get; set; }
  }

  public class ReportResult
  {
    [JsonProperty("data_list")]
    public List<ReportDetail> DataList { get; set; }

    [JsonProperty("size")]
    public int Size { get; set; }

    [JsonProperty("next_cursor")]
    public long NextCursor { get; set; }

    [JsonProperty("has_more")]
    public bool HasMore { get; set; }
  }

  public class ReportDetail
  {
    [JsonProperty("contents")]
    public List<JsonObject> Contents { get; set; }

    [JsonProperty("remark")]
    public string Remark       { get; set; }

    [JsonProperty("template_name")]
    public string TemplateName { get; set; }

    [JsonProperty("dept_name")]
    public string DeptName     { get; set; }

    [JsonProperty("creator_name")]
    public string CreatorName  { get; set; }

    [JsonProperty("creator_id")]
    public string CreatorId    { get; set; }

    [JsonProperty("create_time")]
    public long CreateTime   { get; set; }

    [JsonProperty("report_id")]
    public string ReportId     { get; set; }

    [JsonProperty("modified_time")]
    public long ModifiedTime { get; set; }
  }
}