using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Monkey.Dingtalk.SDK.Report.Dto;

namespace Monkey.Dingtalk.SDK.Report
{
  /// <summary>
  /// 钉钉-日志
  /// </summary>
  public class ReportApi : ApiBase
  {
    public ReportApi(ILogger<ApiBase> logger, IServiceProvider serviceProvider) : base(logger, serviceProvider) { }

    /// <summary>
    /// 企业使用此接口查询用户某个模板的日志情况，进行统计分析，也可以与自有业务系统对接，将日志的表单数据映射到业务系统。
    /// 企业可以根据员工userid或者日志模板名称，分页获取员工一段时间范围内钉钉日志应用发送的日志详细信息。
    /// </summary>
    /// <param name="startTime">起始时间。时间的毫秒数</param>
    /// <param name="endTime">截止时间。时间的毫秒数，如：1520956800000</param>
    /// <param name="cursor">查询游标，初始传入0，后续从上一次的返回值中获取</param>
    /// <param name="size">每页数据量, 最大值是20</param>
    /// <param name="templateName">要查询的模板名称</param>
    /// <param name="userid">员工的userid</param>
    public async Task<ReportListResult> List(long    startTime,           long endTime, long cursor = 0, int size = 20,
                                             string? templateName = null, string? userid = null) {
      return await Post<ReportListResult>(
               "topapi/report/list?access_token={ACCESS_TOKEN}",
               new {
                 start_time    = startTime,
                 end_time      = endTime,
                 template_name = templateName,
                 userid,
                 cursor,
                 size,
               }
             );
    }
  }
}