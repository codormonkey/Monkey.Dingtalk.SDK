# 特别声明
### 本仓库 forked from MrFangHua/Magicodes.Dingtalk.SDK
### 由于原仓库似乎停止维护，遂fork了这个版本进行维护升级

# Monkey.Dingtalk.SDK
钉钉SDK .NET Core 版

## 特色

* 编写简单,易于理解
* 支持成功状态判断
* 支持Token管理
* 返回结果命名易于理解并符合C#命名规范

## Nuget

| 名称     |      Nuget      | 
|----------|:-------------:|
| Monkey.Dingtalk.SDK  |  [![NuGet](https://buildstats.info/nuget/Monkey.Dingtalk.SDK)](https://www.nuget.org/packages/Monkey.Dingtalk.SDK) |


## VNext

* 完善接口

## 使用

### 注册服务并且设置服务生命周期

    // 原作者漏了这句，Token缓存（此处使用内存缓存）
    services.AddSingleton<IDistributedCache, MemoryDistributedCache>();
    
    services.AddTransient<TokenApi>();
    services.AddSingleton<TokenManager>();
    services.AddTransient<AttendanceApi>();
    services.AddSingleton<StaffApi>();
    services.AddTransient<DailyApi>();
    services.AddTransient<DepartmentApi>();
    services.AddTransient<NoticeApi>();
    // 省略其他Api，请自行挖掘

### 通过构造函数注入

    private readonly AttendanceApi _attendanceApi;
    private readonly StaffApi _staffApi;

    public ExampleBackgroundServices(AttendanceApi attendanceApi, StaffApi staffApi)
    {
        _attendanceApi = attendanceApi;
        _staffApi = staffApi;
    }

### 使用接口

    //获取排班记录
    var userList = await GetCheduleUserList();
    if (userList == null || userList.Count == 0)
    {
        _logger.LogWarning("未能获取到考勤人员！");
        return;
    }